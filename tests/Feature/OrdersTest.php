<?php

namespace Tests\Feature;

use App\Order;
use App\Product;
use App\Status;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrdersTest extends TestCase
{

    /**
     * список заказов
     *
     * @return void
     */
    public function testBasicTest()
    {
        $user=User::first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->get('/api/orders');

        $response->assertStatus(200);
    }

    /**
     * создание заказа
     */
    public function testNewOrderTest()
    {
        $user=User::first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->post('/api/orders',['address'=>'test address','comment'=>'test comment','buyer'=>$user->id]);

        $response->assertStatus(200);
    }

    /**
     * создание заказа
     */
    public function testEditOrderTest()
    {
        $user=User::first();
        $order=Order::latest()->first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->put('/api/orders/'.$order->id,['address'=>'test address','comment'=>'test comment']);

        $response->assertStatus(200);
    }

    /**
     * добавление товара
     */
    public function testAddProduct()
    {
        $user=User::first();
        $order=Order::latest()->first();
        $product=Product::inRandomOrder()->first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->put('/api/orders/add_product/'.$order->id,['product_id'=>$product->id,'count'=>1]);

        $response->assertStatus(200);
    }

    /**
     * меняем статус заказа
     */
    public function testChangeStatusTest()
    {
        $user=User::first();
        $order=Order::latest()->first();
        $status=Status::where('position','>',1)->first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->put('/api/orders/status/'.$order->id,['status_id'=>$status->id]);

        $response->assertStatus(200);
    }

    /**
     * удаляем заказ
     */
    public function testDeleteOrderTest()
    {
        $user=User::first();
        $order=Order::latest()->first();
        $this->actingAs($user, 'api_user');
        $this->withoutMiddleware();
        $response = $this->delete('/api/orders/'.$order->id);

        $response->assertStatus(200);

    }
}
