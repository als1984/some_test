<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Вход и регистрация
 */
Route::post('/login', 'Api\UserController@login');

Route::post('/register', 'Api\UserController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api_user'], function() {

    /**
     * Products CRUD
     */
    Route::get('products','Api\ProductsController@index');

    Route::get('products/{id}','Api\ProductsController@show');

    Route::group(['middleware'=>'check.role:manager'],function (){
        Route::post('products','Api\ProductsController@store');
        Route::put('products/{id}','Api\ProductsController@update');
    });
    Route::group(['middleware'=>'check.role:admin'],function (){
        Route::delete('products/{id}','Api\ProductsController@destroy');
    });

    /**
     * Statuses CRUD
     */
    Route::group(['middleware'=>'check.role:manager'],function (){
        Route::get('statuses','Api\StatusesController@index');
    });
    Route::group(['middleware'=>'check.role:admin'],function (){
        Route::get('statuses/{id}','Api\StatusesController@show');
        Route::post('statuses','Api\StatusesController@store');
        Route::put('statuses/{id}','Api\StatusesController@update');
        Route::delete('statuses/{id}','Api\StatusesController@destroy');
    });

    /**
     * Orders CRUD
     */
    Route::get('orders','Api\OrdersController@index');

    Route::get('orders/{id}','Api\OrdersController@show');

    Route::post('orders','Api\OrdersController@store');
    Route::put('orders/{id}','Api\OrdersController@update');
    Route::put('orders/add_product/{id}','Api\OrdersController@addProduct');
    Route::put('orders/del_product/{id}','Api\OrdersController@delProduct');
    Route::put('orders/status/{id}','Api\OrdersController@changeStatus');

    Route::group(['middleware'=>'check.role:admin'],function (){
        Route::delete('orders/{id}','Api\OrdersController@destroy');
    });
});
