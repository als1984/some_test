<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *  definition="User",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      description="логин"
 *  ),
 *  @SWG\Property(
 *      property="email",
 *      type="string",
 *      description="email"
 *  ),
 *  @SWG\Property(
 *      property="password",
 *      type="string",
 *      description="пароль не выводится в ответе"
 *  ),
 *  @SWG\Property(
 *      property="token",
 *      type="string",
 *      description="Bearer token получается при входе и регистрации"
 *  )
 * )
 */
class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'roles_users','user_id','role_id');
    }

    /**
     * есть ли роль у юзера
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role_name)
    {
        return (boolean)$this->roles()->where('name','=',$role_name)->count();
    }

    /**
     * добавить роль юзеру
     * @param string $role_name
     * @return bool
     */
    public function setRole(string $role_name)
    {
        if(!$this->roles()->where('name','=',$role_name)->count()){
            $role=Role::where('name','=',$role_name)->firstOrFail();
            $this->roles()->attach($role);
        }
        return true;
    }

    /**
     * убрать роль у юзера
     * @param string $role_name
     * @return bool
     */
    public function deleteRole(string $role_name)
    {
        $role=$this->roles()->where('name','=',$role_name)->firstOrFail();
        if($role){
            $this->roles()->detach($role);
            return true;
        }
        return false;
    }

}
