<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class GetOrderInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:info {order} {--products}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_id=$this->argument('order');
        $take_products = $this->option('products');
        $order=Order::find($order_id);
        if(!$order){
            $this->error('Заказ с id='.$order_id.' не найден');
            return;
        }

        //$order_wp=Order::find($order_id)->pluck(['id','user_id','status_id','buyer','edited_by','address','comment','created_at','updated_at','summ'])->toArray();
        $this->table(['id',
            'user_id',
            'status_id',
            'buyer',
            'edited_by',
            'purchase',
            'address',
            'comment',
            'created_at',
            'updated_at',
            'summ'],[$order->toArray()]);
        if(!$take_products){
            $purchase=json_decode($order->purchase,true);
            if($purchase) {
                $this->comment('Данные о товарах (сохраненые)');
                $this->table(['id', 'name', 'count', 'price'], $purchase);
            }
        }else {
            $this->comment('Данные о товарах (из таблицы товаров)');
            $products=$order->products()->get();
            $products_arr=[];
            foreach ($products as $product){
                $products_arr[]=['id'=>$product->id,
                    'name'=>$product->name,
                    'price'=>$product->price,
                    'inventory'=>$product->inventory,
                    'count'=>$product->pivot->count];
            }

            $this->table(['id','name','price','inventory','count'],$products_arr);
        }
    }
}
