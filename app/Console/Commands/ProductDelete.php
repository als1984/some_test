<?php

namespace App\Console\Commands;

use App\Order;
use App\Product;
use App\Traits\ChangePurchase;
use Illuminate\Console\Command;

class ProductDelete extends Command
{
    use ChangePurchase;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:del_product {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалим товар из заказа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_id=$this->argument('order');
        $order=Order::find($order_id);
        if(!$order){
            $this->error('Заказ с id='.$order_id.' не найден');
            return;
        }
        $product_id=$this->ask('Введите id товара');
        $count=$this->ask('Введите кол-во товара');
        $product=Product::find($product_id);
        if(!$order){
            $this->error('Товар с id='.$product_id.' не найден');
            return;
        }
        if($product->inventory<$count){
            $this->error( 'на складе нет данного количества');
            return;
        }
        $order_product=$order->products()->find($product->id);
        if($order_product){
            if($order_product->pivot->count<=$count){
                $order->products()->detach($product);
                $purchase=$this->ChangePurchase(json_decode($order->purchase,true),$product,0);
                $order->purchase=json_encode($purchase);
            }else{
                $order_product->pivot->count-=$count;
                $order_product->pivot->save();
                $purchase=$this->ChangePurchase(json_decode($order->purchase,true),$product,$order_product->pivot->count);
                $order->purchase=json_encode($purchase);
            }
        }else{
            return response()->json(['Product is not found'],404);
        }
        $summ=$product->price*$count;
        $order->decrement('summ',$summ);
        $order->save();
        if($order->status->position>1 && !$order->status->status_cancelled){
            $product->increment('inventory',$count);
        }
        $order_arr=$order->toArray();
        unset($order_arr['status']);
        unset($order_arr['purchase']);
        $this->table(['id',
            'user_id',
            'status_id',
            'buyer',
            'edited_by',
            'address',
            'comment',
            'created_at',
            'updated_at',
            'summ'],[$order_arr]);
        $purchase=json_decode($order->purchase,true);
        if($purchase) {
            $this->comment('Данные о товарах (сохраненые)');
            $this->table(['id', 'name', 'count', 'price'], $purchase);
        }
    }
}
