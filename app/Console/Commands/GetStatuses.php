<?php

namespace App\Console\Commands;

use App\Status;
use Illuminate\Console\Command;

class GetStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statuses:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $statuses=Status::all()->toArray();
        $this->table(['id','name','position','status_done','status_cancelled','can_cancel'],$statuses);
    }
}
