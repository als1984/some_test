<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class DeleteOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:delete {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удалить заказ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_id=$this->argument('order');
        $order=Order::find($order_id);
        if(!$order){
            $this->error('Заказ с id='.$order_id.' не найден');
            return;
        }
        if($order->status->position>1 && $order->status->status_cancelled){
            $products=$order->products()->get();
            foreach ($products as $product){
                $product->increment('inventory',$product->pivot->count);
            }
        }
        $order->delete();
        $this->comment('Заказ удален');

    }
}
