<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;

class GetProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Список товаров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products=Product::all()->toArray();
        $this->table(['id','name','price','inventory'],$products);
    }
}
