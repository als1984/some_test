<?php

namespace App\Console\Commands;

use App\Order;
use App\Status;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class NewOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $address=$this->ask('Введите адрес доставки');
        $comment=$this->ask('Введите комментарий');
        $buyer=$this->ask('Введите id покупателя');
        $user_buyer=User::find($buyer);
        if(!$user_buyer){
            $this->error('Покупатель с id='.$buyer.' не найден');
            return;
        }
        $data=compact('address','buyer','comment');
        $user=User::whereHas('roles', function (Builder $query) {
            $query->where('name', '=', 'admin');
        })->first();
        $status=Status::where('position','=',1)->first();
        $data['status_id']=$status->id;
        $data['user_id']=$user->id;
        $data['edited_by']=$user->id;
        $order=new Order($data);
        $order->save();
        $this->table(['id','buyer','address','comment','user_id','edited_by','created_at','updated_at','status_id'],[$order->toArray()]);
    }
}
