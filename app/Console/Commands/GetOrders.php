<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;

class GetOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Список заказов';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders=Order::all()->toArray();

        $this->table(['id','user_id','status_id','buyer','edited_by','purchase','address','comment','created_at','updated_at','summ'],$orders);
    }
}
