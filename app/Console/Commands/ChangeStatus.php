<?php

namespace App\Console\Commands;

use App\Order;
use App\Status;
use App\Traits\CheckStatusTrait;
use Illuminate\Console\Command;

class ChangeStatus extends Command
{
    use CheckStatusTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status {order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'меняем статус заказа и списываем товары со склада';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order_id = $this->argument('order');
        $order = Order::find($order_id);
        if (!$order) {
            $this->error('Заказ с id=' . $order_id . ' не найден');
            return;
        }
        $status_id = $this->ask('Введите id статуса');
        $status = Status::find($status_id);
        if (!$status) {
            $this->error('Статус с id=' . $status_id . ' не найден');
            return;
        }

        if (!$this->canUpdateStatus($status, $order)) {
            $this->error('Нельзя заказу со статусом ' . $order->status->name . ' установить статус ' . $status->name);
            return;
        }
        //списание или возврат товаров
        if ($order->status->position == 1 && !$status->status_cancelled) {
            $products = $order->products()->get();
            foreach ($products as $product) {
                $product->decrement('inventory', $product->pivot->count);
            }
        }
        if ($order->status->position >= 1 && $status->status_cancelled) {
            $products = $order->products()->get();
            foreach ($products as $product) {
                $product->increment('inventory', $product->pivot->count);
            }
        }
        $order->status_id = $status->id;
        $order->save();
        $order_arr = $order->toArray();
        unset($order_arr['status']);
        unset($order_arr['purchase']);
        $this->table(['id', 'user_id', 'status_id', 'buyer', 'edited_by', 'address', 'comment', 'created_at', 'updated_at', 'summ'], [$order_arr]);
    }
}
