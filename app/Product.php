<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Product",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      description="Название товара"
 *  ),
 *  @SWG\Property(
 *      property="price",
 *      type="number",
 *      description="Цена товара"
 *  ),
 *  @SWG\Property(
 *      property="inventory",
 *      type="integer",
 *      description="Кол-во на складе"
 *  )
 * )
 */
class Product extends Model
{
    protected $fillable=['name','price','inventory'];

}
