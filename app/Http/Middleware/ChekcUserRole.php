<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ChekcUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user=Auth::user();
        if(!$user->hasRole($role)){
            return response()->json(['messages.no_permissions'],401);
        }
        return $next($request);
    }
}
