<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/products",
     *     summary="Получаем список товаров",
     *     tags={"Products"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Product")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::all();
        return response()->json(['success'=>$products]);
    }

    /**
     * @SWG\Post(
     *     path="/api/products",
     *     summary="Добавить товар",
     *     tags={"Products"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         description="Название товара",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="price",
     *         in="query",
     *         description="Цена товара",
     *         required=true,
     *         type="number",
     *     ),
     *     @SWG\Parameter(
     *         name="inventory",
     *         in="query",
     *         description="Количество товара",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Product"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'required|string',
            'price'=>'required|numeric',
            'inventory'=>'nullable|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $product=new Product($request->all());
        $product->save();
        return response()->json(['success'=>$product],200);
    }

    /**
     * @SWG\Get(
     *     path="/api/products/{product_id}",
     *     summary="Получаем информацию о товаре",
     *     tags={"Products"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="path",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Product"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Product is not found",
     *     )
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$id || !(int)$id){
            return response()->json(['Product is not found'],404);
        }
        $product=Product::find($id);
        if(!$product){
            return response()->json(['Product is not found'],404);
        }
        return response()->json(['success'=>$product]);
    }

    /**
     * @SWG\Put(
     *     path="/api/products/{product_id}",
     *     summary="Редактировать товар",
     *     tags={"Products"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="path",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         description="Название товара",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="price",
     *         in="query",
     *         description="Цена товара",
     *         required=false,
     *         type="number",
     *     ),
     *     @SWG\Parameter(
     *         name="inventory",
     *         in="query",
     *         description="Количество товара",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Product"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Product is not found",
     *     )
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'name'=>'nullable|string',
            'price'=>'nullable|numeric',
            'inventory'=>'nullable|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $product=Product::find($id);
        if(!$product){
            return response()->json(['Product is not found'],404);
        }
        $data=$request->all();
        foreach ($data as $key=>$val){
            $product->$key=$val;
        }
        $product->save();
        return response()->json(['success'=>$product]);
    }

    /**
     * @SWG\Delete(
     *     path="/api/products/{product_id}",
     *     summary="Удалить товар",
     *     tags={"Products"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="path",
     *         description="Product id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Товар удален",
     *
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Product is not found",
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        if(!$product){
            return response()->json(['Product is not found'],404);
        }
        $product->delete();
        return response()->json(['Товар удален'],200);
    }
}
