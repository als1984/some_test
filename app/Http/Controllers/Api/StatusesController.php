<?php

namespace App\Http\Controllers\Api;

use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StatusesController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/statuses",
     *     summary="Получаем список статусов",
     *     tags={"Statuses"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Status")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses=Status::all();
        return response()->json(['success'=>$statuses]);
    }

    /**
     * @SWG\Post(
     *     path="/api/statuses",
     *     summary="Добавить статус",
     *     tags={"Statuses"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         description="Название статуса",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="position",
     *         in="query",
     *         description="Позиция статуса",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="status_done",
     *         in="query",
     *         description="Это последний статус? (Выполнен)",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Parameter(
     *         name="can_cancel",
     *         in="query",
     *         description="Это статус можно отменять?",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Parameter(
     *         name="status_cancelled",
     *         in="query",
     *         description="Это отмененный статус? (Отменен)",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Status"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$this->stringToBool($request->all());

        $validator=Validator::make($data,[
            'name'=>'required|string',
            'position'=>'required|integer',
            'status_done'=>'nullable|boolean',
            'can_cancel'=>'nullable|boolean',
            'status_cancelled'=>'nullable|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $status=new Status($data);
        $status->save();
        $this->incrementPositions($data['position']);
        return response()->json(['success'=>$status],200);
    }

    /**
     * @SWG\Get(
     *     path="/api/statuses/{status_id}",
     *     summary="Получаем информацию о статусе",
     *     tags={"Statuses"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="status_id",
     *         in="path",
     *         description="Status id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Status"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Status is not found",
     *     )
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$id || !(int)$id){
            return response()->json(['Status is not found'],404);
        }
        $status=Status::find($id);
        if(!$status){
            return response()->json(['Status is not found'],404);
        }
        return response()->json(['success'=>$status]);
    }

    /**
     * @SWG\Put(
     *     path="/api/statuses/{status_id}",
     *     summary="Обновить статус",
     *     tags={"Statuses"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="status_id",
     *         in="path",
     *         description="Status id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         description="Название статуса",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="position",
     *         in="query",
     *         description="Позиция статуса",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="status_done",
     *         in="query",
     *         description="Это последний статус? (Выполнен)",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Parameter(
     *         name="can_cancel",
     *         in="query",
     *         description="Это статус можно отменять?",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Parameter(
     *         name="status_cancelled",
     *         in="query",
     *         description="Это отмененный статус? (Отменен)",
     *         required=false,
     *         type="boolean",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Status"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     )
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$this->stringToBool($request->all());

        $validator=Validator::make($data,[
            'name'=>'required|string',
            'position'=>'required|integer',
            'status_done'=>'nullable|boolean',
            'can_cancel'=>'nullable|boolean',
            'status_cancelled'=>'nullable|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $status=Status::find($id);
        if(!$status){
            return response()->json(['Status is not found'],404);
        }
        $status->save($data);
        if(isset($data['position']) && $data['position']!=$status->position) {
            $this->dercementPositions($status->position);
            $this->incrementPositions($data['position']);
            $status->position=$data['position'];
            $status->save();
        }


        return response()->json(['success'=>$status],200);
    }

    /**
     * @SWG\Delete(
     *     path="/api/statuses/{status_id}",
     *     summary="Удалить статус",
     *     tags={"Statuses"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="status_id",
     *         in="path",
     *         description="Status id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Статус удален",
     *
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Status is not found",
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status=Status::find($id);
        if(!$status){
            return response()->json(['Status is not found'],404);
        }
        $position=$status->position;
        $status->delete();
        $this->dercementPositions($position);
        return response()->json(['Статус удален'],200);
    }

    /**
     * пересчет позиций статусов после добавления
     */
    public function incrementPositions(int $position)
    {
        $statuses=Status::where('position','>=',$position)->orderBy('position','asc')->get();
        foreach ($statuses as $status){
            $status->increment('position');
            $status->save();
        }
    }

    /**
     * пересчет позиций статусов после удаления
     */
    public function dercementPositions(int $position)
    {
        $statuses=Status::where('position','>',$position)->orderBy('position','asc')->get();
        foreach ($statuses as $status){
            $status->decrement('position');
            $status->save();
        }
    }

    /**
     * превращаем строки 'true', 'false' в boolean (т.к. swagger отсылает строку а она не проходит валидацию)
     * @param $data
     * @return mixed
     */
    public function stringToBool($data)
    {
        foreach ($data as $key=>$val){
            if($val=='true'){
                $data[$key]=true;
            }
            if($val=='false'){
                $data[$key]=false;
            }
        }
        return $data;
    }

}
