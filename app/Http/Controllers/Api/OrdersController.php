<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\Product;
use App\Status;
use App\Traits\ChangePurchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\CheckStatusTrait;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    use CheckStatusTrait,ChangePurchase;

    /**
     * @SWG\Get(
     *     path="/api/orders",
     *     summary="Получаем список заказов, если это пользователь, то только его заказы, если менеджер то все",
     *     tags={"Orders"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Order")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=auth()->user();
        if($user->hasRole('manager')){
            $orders=Order::all();
        }else{
            $orders=$user->orders()->get();
        }
        return response()->json(['succcess'=>$orders]);
    }

    /**
     * @SWG\Post(
     *     path="/api/orders",
     *     summary="Создать заказ (создает пустой заказ со статусом (Создан))",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="buyer",
     *         in="query",
     *         description="ID покупателя",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="address",
     *         in="query",
     *         description="Адрес доставки",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="comment",
     *         in="query",
     *         description="Комментарий",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();
        $validator=Validator::make($data,[
            'buyer'=>'required|integer',
            'address'=>'required|string',
            'comment'=>'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $user=auth()->user();
        $status=Status::where('position','=',1)->first();
        $data['status_id']=$status->id;
        $data['user_id']=$user->id;
        $data['edited_by']=$user->id;
        $order=new Order($data);
        $order->save();
        return response()->json(['success'=>$order]);
    }

    /**
     * @SWG\Get(
     *     path="/api/orders/{order_id}",
     *     summary="Получаем информацию о заказе (для менеджера ищем по всем заказам для юзера только его)",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="Order id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Order is not found",
     *     )
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=auth()->user();
        if($user->hasRole('manager')){
            $order=Order::find($id);
        }else{
            $order=$user->orders()->find($id);
        }
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        return response()->json(['success'=>$order]);
    }


    /**
     * @SWG\Put(
     *     path="/api/orders/{order_id}",
     *     summary="Редактируем заказ (меняем адрес доставки, покупателя, комментарий)",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="ID заказа",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="buyer",
     *         in="query",
     *         description="ID покупателя",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="address",
     *         in="query",
     *         description="Адрес доставки",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="comment",
     *         in="query",
     *         description="Комментарий",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Order is not found",
     *     )
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->all();
        $validator=Validator::make($data,[
            'buyer'=>'nullable|integer',
            'address'=>'nullable|string',
            'comment'=>'nullable|string',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $user=auth()->user();
        if(isset($data['buyer'])){
            if(!$user->hasRole('manager') && $data['buyer']!=$user->id){
                return response()->json(['error' => 'id покупателя не верный'], 400);
            }
        }

        if($user->hasRole('manager')){
            $order=Order::find($id);
        }else{
            $order=$user->orders()->find($id);
        }
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        $data['edited_by']=$user->id;
        $order->update($data);
        return response()->json(['success'=>$order],200);
    }

    /**
     * @SWG\Delete(
     *     path="/api/orders/{order_id}",
     *     summary="Удалить заказ",
     *     tags={"Orders"},
     *     security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="Order id",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Заказ удален",
     *
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Status is not found",
     *     )
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order=Order::find($id);
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        if($order->status->position>1 && !$order->status->status_cancelled){
            $products=$order->products()->get();
            foreach ($products as $product){
                $product->increment('inventory',$product->pivot->count);
            }
        }
        $order->delete();
        return response()->json(['Заказ удален'],200);
    }

    /**
     * @SWG\Put(
     *     path="/api/orders/add_product/{order_id}",
     *     summary="добавим товар к заказу",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="ID заказа",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="query",
     *         description="ID товара",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="count",
     *         in="query",
     *         description="Количество",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Order or Product is not found",
     *     )
     * )
     */
    /**
     * добавим товар к заказу
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProduct(Request $request,$id)
    {
        $validator=Validator::make($request->all(),[
            'product_id'=>'required|integer',
            'count'=>'required|integer|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $data=$request->all();
        $user=auth()->user();
        if($user->hasRole('manager')){
            $order=Order::find($id);
        }else{
            $order=$user->orders()->find($id);
        }
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        $product=Product::find($data['product_id']);
        if(!$product){
            return response()->json(['Product is not found'],404);
        }
        if($product->inventory<$data['count']){
            return response()->json(['error' => 'на складе нет данного количества'], 400);
        }
        if(!$user->hasRole('manager') && $order->status->position>1){
            return response()->json(['error'=>'Вы не можете редактировать данный заказ '],401);
        }
        $order_product=$order->products()->find($product->id);
        if($order_product){
            $order_product->pivot->count+=$data['count'];
            $order_product->pivot->save();
        }else{
            $order->products()->attach($product,['count'=>$data['count']]);
            $order_product=$order->products()->find($product->id);
        }
        $summ=$product->price*$data['count'];
        $order->increment('summ',$summ);
        $purchase=$this->ChangePurchase(json_decode($order->purchase,true),$product,$order_product->pivot->count);
        $order->purchase=json_encode($purchase);
        $order->save();
        if($order->status->position>1 && !$order->status->status_cancelled){
            $product->decrement('inventory',$data['count']);
        }
        return response()->json(['success'=>$order],200);
    }

    /**
     * @SWG\Put(
     *     path="/api/orders/del_product/{order_id}",
     *     summary="Удалим товар из заказа",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="ID заказа",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="product_id",
     *         in="query",
     *         description="ID товара",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="count",
     *         in="query",
     *         description="Количество",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации, либо пользователь пытаается отредактировать заказ у которого позиция статуса > 1",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Order or Product is not found",
     *     )
     * )
     */
    /**
     * добавим товар к заказу
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delProduct(Request $request,$id)
    {
        $validator=Validator::make($request->all(),[
            'product_id'=>'required|integer',
            'count'=>'required|integer|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $data=$request->all();
        $user=auth()->user();
        if($user->hasRole('manager')){
            $order=Order::find($id);
        }else{
            $order=$user->orders()->find($id);
        }
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        if(!$user->hasRole('manager') && $order->status->position>1){
            return response()->json(['error'=>'Вы не можете редактировать данный заказ '],401);
        }
        $product=Product::find($data['product_id']);
        if(!$product){
            return response()->json(['Product is not found'],404);
        }
        $order_product=$order->products()->find($product->id);
        if($order_product){
            if($order_product->pivot->count<=$data['count']){
                $order->products()->detach($product);
                $purchase=$this->ChangePurchase(json_decode($order->purchase,true),$product,0);
                $order->purchase=json_encode($purchase);
            }else{
                $order_product->pivot->count-=$data['count'];
                $order_product->pivot->save();
                $purchase=$this->ChangePurchase(json_decode($order->purchase,true),$product,$order_product->pivot->count);
                $order->purchase=json_encode($purchase);
            }
        }else{
            return response()->json(['Product is not found'],404);
        }
        $summ=$product->price*$data['count'];
        $order->decrement('summ',$summ);
        $order->save();
        if($order->status->position>1 && !$order->status->status_cancelled){
            $product->increment('inventory',$data['count']);
        }
        return response()->json(['success'=>$order],200);
    }

    /**
     * @SWG\Put(
     *     path="/api/orders/status/{order_id}",
     *     summary="меняем статус заказа и списываем товары со склада",
     *     tags={"Orders"},
     *      security={
     *          {"passport": {}},
     *     },
     *     @SWG\Parameter(
     *         name="order_id",
     *         in="path",
     *         description="ID заказа",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="status_id",
     *         in="query",
     *         description="ID статуса",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Order"),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user или у пользователя нет прав на смену статуса заказа",
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Ошибка валидации, либо пользователь хочет перескочить через позицию в статусе",
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Order or Status is not found",
     *     )
     * )
     */
    /**
     * меняем статус заказа и списываем товары со склада
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(Request $request,$id)
    {
        $validator=Validator::make($request->all(),[
            'status_id'=>'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $data=$request->all();
        $user=auth()->user();
        if($user->hasRole('manager')){
            $order=Order::find($id);
        }else{
            $order=$user->orders()->find($id);
        }
        if(!$order){
            return response()->json(['Order is not found'],404);
        }
        $status=Status::find($data['status_id']);
        if(!$status){
            return response()->json(['Status is not found'],404);
        }
        if(!$user->hasRole('manager') && !$status->status_cancelled){
            return response()->json(['Можно только отменить заказ'],401);
        }
        if(!$this->canUpdateStatus($status,$order)){
            return response()->json(['error' => 'Нельзя заказу со статусом '.$order->status->name.' установить статус '.$status->name], 400);
        }
        //списание или возврат товаров
        if($order->status->position==1 && !$status->status_cancelled){
            $products=$order->products()->get();
            foreach ($products as $product){
                $product->decrement('inventory',$product->pivot->count);
            }
        }
        if($order->status->position>=1 && $status->status_cancelled){
            $products=$order->products()->get();
            foreach ($products as $product){
                $product->increment('inventory',$product->pivot->count);
            }
        }
        $order->status_id=$status->id;
        $order->save();
        return response()->json(['success'=>$order],200);
    }
}
