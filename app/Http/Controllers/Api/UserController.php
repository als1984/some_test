<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * @SWG\Post(
     *     path="/api/login",
     *     summary="Login",
     *     tags={"Login"},
     *    @SWG\Parameter(
     *     in="query",
     *     name="email",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="password",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="Пользователь залогинен",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *              title="token",
     *              description="Bearer token",
     *              type="string"
     *              )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     * )
     */
    /**
     * переопределим стандартный метод входа
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        if (Auth::attempt(
            [
                'email' => request('email'),
                'password' => request('password')
            ]
        )) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('SmartSatu')->accessToken;
            return response()->json(['success' => $success], 200);
        } else {
            return response()->json(['error' => __('auth.failed')], 401);
        }
    }

    /**
     * @SWG\Post(
     *     path="/api/register",
     *     summary="Register",
     *     tags={"Register"},
     *    @SWG\Parameter(
     *     in="query",
     *     name="name",
     *     description="Имя пользователя",
     *     required=true,
     *     type="string"
     *   ),
     *    @SWG\Parameter(
     *     in="query",
     *     name="email",
     *     description="Email",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="password",
     *     description="Password",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Parameter(
     *     in="query",
     *     name="password_confirmation",
     *     description="Повторить пароль",
     *     required=true,
     *     type="string"
     *   ),
     *     @SWG\Response(
     *         response=200,
     *         description="Пользователь зарегистрирован",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(
     *              title="token",
     *              description="Bearer token",
     *              type="string"
     *              )
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     ),
     * )
     */
    /**
     * переопределим стандартный метод регистрации
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $input = $request->all();
        $input['password'] = \Illuminate\Support\Facades\Hash::make($input['password']);
        $user = User::create($input);
        $user->setRole('user');
        $success['token'] =  $user->createToken('SmartSatu')-> accessToken;

        return response()->json(['success' => $success], 200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO управление пользователями
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO управление пользователями
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO управление пользователями
    }
}
