<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Status",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="name",
 *      type="string",
 *      description="Название статуса"
 *  ),
 *  @SWG\Property(
 *      property="position",
 *      type="integer",
 *      description="Позиция статуса (статус меняется в определенном порядке)"
 *  ),
 *  @SWG\Property(
 *      property="status_done",
 *      type="boolean",
 *      description="Это последний статус? (Выполнен)"
 *  ),
 *     @SWG\Property(
 *      property="status_cancelled",
 *      type="boolean",
 *      description="Это отмененный статус? (Отменен)"
 *  ),
 *     @SWG\Property(
 *      property="can_cancel",
 *      type="boolean",
 *      description="Это статус можно отменять? (Для того чтобы пользователь не мог отменить отправленный либо доставленный заказ)"
 *  )
 * )
 */
class Status extends Model
{
    protected $fillable = ['name', 'position', 'status_done', 'status_cancelled', 'can_cancel'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'status_id');
    }

}
