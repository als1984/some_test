<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(
 *  definition="Order",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="purchase",
 *      type="array",
 *      description="Данные зазаза в json",
 *      @SWG\Items(
 *             type="string",
 *         ),
 *  ),
 *  @SWG\Property(
 *      property="summ",
 *      type="number",
 *      description="Общая стоимость заказа"
 *  ),
 *  @SWG\Property(
 *      property="user_id",
 *      type="integer",
 *      description="ID пользователя создавшего заказ"
 *  ),
 *  @SWG\Property(
 *      property="edited_by",
 *      type="integer",
 *      description="ID пользователя изменившего заказ"
 *  ),
 *  @SWG\Property(
 *      property="buyer",
 *      type="integer",
 *      description="ID покупателя"
 *  ),
 *  @SWG\Property(
 *      property="address",
 *      type="string",
 *      description="Адрес доставки"
 *  ),
 *  @SWG\Property(
 *      property="comment",
 *      type="string",
 *      description="Комментарий"
 *  )
 * )
 */
class Order extends Model
{
    protected $fillable=['user_id','status_id','buyer',	'edited_by','purchase','address','comment'];

    /**
     * получаем создателя заказа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * товары заказа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class,'orders_products','order_id','product_id')->withPivot('count');
    }

    /**
     * статус заказа
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class,'status_id');
    }

    /**
     * сохраняем товары с ценой и кол-вом в json, на случай измнения цен у товаров или удаления товара
     */
    public function setPurchase()
    {
        $products=$this->products();
        $purchase=[];
        foreach ($products as $product){
            $purchase[]=['product_id'=>$product->id,'product_name'=>$product->name,'product_price'=>$product->price,'product_count'=>$product->count];
        }
        $this->purchase=json_encode($purchase);
        $this->save();
    }

    /**
     * установим статус
     * @param Status $status
     */
    public function setStatus(Status $status)
    {
        $this->status_id=$status->id;
        $this->save();
    }


}
