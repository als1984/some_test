<?php
/**
 * Created by PhpStorm.
 * User: sanek
 * Date: 28.07.19
 * Time: 14:53
 */

namespace App\Traits;


use App\Product;

trait ChangePurchase
{
    /**
     * пересчитываем json с покупками в заказе
     * @param $purchases
     * @param Product $product
     * @param $count
     * @return array
     */
    public function ChangePurchase($purchases, Product $product,$count)
    {
        if(!$purchases && $count>0){
            $purchases=[['id'=>$product->id,'price'=>$product->price,'name'=>$product->name,'count'=>$count]];
        }else{
            //dd($purchases);
            $in_array=-1;
            foreach ($purchases as $key=>$purchase){
                if($purchase['id']==$product->id){
                    $in_array=$key;
                }
            }
            if($in_array>-1){
                if($count) {
                    $purchases[$in_array]['count'] = $count;
                }else{
                    unset($purchases[$in_array]);
                }
            }else{
                $purchases[]=['id'=>$product->id,'price'=>$product->price,'name'=>$product->name,'count'=>$count];
            }
        }
        return $purchases;

    }
}
