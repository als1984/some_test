<?php
/**
 * Created by PhpStorm.
 * User: sanek
 * Date: 28.07.19
 * Time: 13:28
 */

namespace App\Traits;


use App\Order;
use App\Status;
use Illuminate\Support\Facades\Auth;

trait CheckStatusTrait
{
    /**
     * проверяем статусы заказа перед обновлением, можем ли его сменить
     * если заказ выполнен, то вернем false
     * если пытаемся отменить заказ, а его нельзя отменять, то вернем false
     * если не пытаемся отменить заказ, то проверяем разницу в 1 позицию между старым и новым статусами
     * @param Status $status
     * @param Order $order
     * @return bool
     */
    public function canUpdateStatus(Status $status, Order $order)
    {
        $order_status=$order->status;
        if($order_status->status_done){
            return false;
        }
        if($status->status_cancelled && !$order_status->can_cancel){
            return false;
        }
        if(!$status->status_cancelled){
            if(($status->position+1==$order_status->position) || ($status->position-1==$order_status->position)){
                return true;
            }else{
                return false;
            }
        }
        if($status->status_cancelled){
            return true;
        }
        return false;
    }
}
