<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create(['name'=>'Администратор'])->each(function ($user) {
            $user->setRole('admin');
            $user->setRole('manager');
            $user->setRole('user');
        });
        factory(App\User::class, 1)->create(['name'=>'Менеджер'])->each(function ($user) {
            $user->setRole('manager');
            $user->setRole('user');
        });
        factory(App\User::class, 1)->create(['name'=>'Покупатель'])->each(function ($user) {
            $user->setRole('user');
        });
    }
}
