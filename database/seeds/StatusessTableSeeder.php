<?php

use Illuminate\Database\Seeder;

class StatusessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(['name'=>'Создан',
            'can_cancel'=>true,
            'position'=>1,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()]);
        DB::table('statuses')->insert(['name'=>'Обработан',
            'can_cancel'=>true,
            'position'=>2,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()]);
        DB::table('statuses')->insert(['name'=>'Передан курьеру',
            'can_cancel'=>true,
            'position'=>3,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()]);
        DB::table('statuses')->insert(['name'=>'Выполнен',
            'can_cancel'=>false,
            'position'=>4,
            'status_done'=>true,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()]);
        DB::table('statuses')->insert(['name'=>'Отменен',
            'can_cancel'=>false,
            'position'=>5,
            'status_done'=>false,
            'status_cancelled'=>true,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()]);
    }
}
