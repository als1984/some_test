Инструкция по установке 

стандартная установка для ларавел (https://laravel.com/docs/5.8/deployment)

после нее необнодимо выполнить комманды

1. php artisan passport:keys  - ключи для laravel passport

2. php artisan migrate - миграции

4. php artisan db:seed - начальные данные, впринципе данные о товарах и статусах можно не загружать,
 чтобы этого не делать то нужно из файла smatrsatu_test/database/seeds/DatabaseSeeder.php удалить 2 строки 
 $this->call(ProductsTableSeeder::class); $this->call(StatusessTableSeeder::class); но если запускать тесты, то они нужны

5. php artisan l5-swagger:generate - генерируем API документацию (Swagger) http://sitename.com/api/documentation - страница с документацией,
нужно залогинится, поэтому  http://sitename.com/users - список юзеров (пароль у всех password), 
также нужно настроить base url в файле smatrsatu_test/app/Http/Controllers/Controller.php 13 строка

комманды для консоли:

1. php artisan users:list - Получаем пользователей

2. php artisan products:list - Список товаров

3. php artisan statuses:list - Список статусов

4. php artisan orsers:list - Список заказов

5. php artisan order:create - Создать заказ

6. php artisan order:add_product {order} - где  {order} - id заказа -  Добавить товар к заказу

7. php artisan order:del_product {order} - где {order} - id заказа - Удалить товар из заказа

8. php artisan order:status {order} - где {order} - id заказа - Сменить статус заказа

9. php artisan order:delete {order} - где {order} - id заказа - Удалить заказ


на тесты не хватило времени поэтому только тестируем роуты для заказа и достаточно упрощенно

smatrsatu_test/tests/Feature/OrdersTest.php

локальный .env файл - .env_local - там пару нужных настроек 

